<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
/** Класс первого шага: выбор операции над числами*/
class ActionSelect extends CWizardStep
{
    /** Инициализация, добавление следующего шага, заголовка и подзаголовка */
    function InitStep()
    {
        $this->SetStepID("action_select");
        $this->SetTitle(Loc::getMessage("AS_INIT_TITLE"));
        $this->SetSubTitle(Loc::getMessage("AS_INIT_SUBTITLE"));
        $this->SetNextStep("first_number");
    }

    /** Метод вывода содержимого шага: вывод формы для выбора действия из списка*/
    function ShowStep()
    {
        $this->content .= "<table class='wizard-data-table'>";
        $this->content .= "<tr><th align = 'right'><span class='wizard-required'>*</span>" . Loc::getMessage('AS_CHOOSE_ACTION') . "</th><td>";
        $ar_actions = array(
            Loc::getMessage("AS_ADDITION"),
            Loc::getMessage("AS_SUBSTRACTION"),
            Loc::getMessage("AS_MULTIPLICATION"),
            Loc::getMessage("AS_DIVISION"),
        );
        $this->content .= $this->ShowSelectField("ACTION", $ar_actions) . "</td></tr>";
        $this->content .= "</table>";
    }
}

/** Класс второго шага: ввод первого числа */
class FirstNumber extends CWizardStep
{
    /** Инициализация, добавление следующего и предыдущего шага, заголовка и подзаголовка */
    function InitStep()
    {
        $this->SetStepID("first_number");

        $this->SetTitle(Loc::getMessage("FN_INIT_TITLE"));
        $this->SetSubTitle(Loc::getMessage("FN_INIT_SUBTITLE"));

        $this->SetPrevStep("action_select");
        $this->SetNextStep("second_number");
    }

    /** Метод вывода содержимого шага: вывод формы для добавления первого числа*/
    function ShowStep()
    {
        $this->content .= "<table class='wizard-data-table'>";
        $this->content .= "<tr><th align = 'right'><span class='wizard-required'>*</span>" . Loc::getMessage('FN_ENTER_NUM') . "</th><td>";
        $this->content .= $this->ShowInputField("text","F_NUMBER") . "</td></tr>";
        $this->content .= "</table>";
    }

    /** Метод проверки перед отправкой формы: проверка введенных данных (являются ли числом) */
    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        if ($wizard->IsCancelButtonClick())
            return;
        $number = $wizard->GetVar("F_NUMBER");
        if (!is_numeric($number))
            $this->SetError(Loc::getMessage('ERROR_INVALID_NUM'));
    }

}

/** Класс третьего шага: ввод второго числа */
class SecondNumber extends CWizardStep
{
    /** Инициализация, добавление следующего и предыдущего шага, заголовка и подзаголовка */
    function InitStep()
    {
        $this->SetStepID("second_number");

        $this->SetTitle(Loc::getMessage("SN_INIT_TITLE"));
        $this->SetSubTitle(Loc::getMessage("SN_INIT_SUBTITLE"));

        $this->SetPrevStep("first_number");
        $this->SetNextStep("show_problem");
    }

    /** Метод вывода содержимого шага: вывод формы для добавления второго числа*/
    function ShowStep()
    {
        $this->content .= "<table class='wizard-data-table'>";
        $this->content .= "<tr><th align = 'right'><span class='wizard-required'>*</span>" . Loc::getMessage('SN_ENTER_NUM') . "</th><td>";
        $this->content .= $this->ShowInputField("text","S_NUMBER") . "</td></tr>";
        $this->content .= "</table>";
    }

    /** Метод проверки перед отправкой формы: проверка введенных данных (являются ли числом) */
    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        if ($wizard->IsCancelButtonClick())
            return;
        $number = $wizard->GetVar("S_NUMBER");
        if (!is_numeric($number))
            $this->SetError(Loc::getMessage('ERROR_INVALID_NUM'));
    }

}

/** Класс четвертого шага: демонстрация выражения и проверка на ошибки */
class ShowProblem extends CWizardStep
{
    /** Инициализация, добавление следующего и предыдущего шага, заголовка и подзаголовка */
    function InitStep()
    {
        $this->SetStepID("show_problem");

        $this->SetTitle(Loc::getMessage("SP_INIT_TITLE"));
        $this->SetSubTitle(Loc::getMessage("SP_INIT_SUBTITLE"));

        $this->SetPrevStep("second_number");
        $this->SetFinishStep("solve");
        $this->SetFinishCaption(Loc::getMessage("SP_INIT_FINISH_CAPTION"));
    }

    /** Метод вывода: запоминание выражения и его вывод на экран */
    function ShowStep()
    {
        $wizard =& $this->GetWizard();
        $wizard->SetVar("OPERATOR","+");
        switch ($wizard->GetVar("ACTION"))
        {
            case 1:
                $wizard->SetVar("OPERATOR","-");
                break;
            case 2:
                $wizard->SetVar("OPERATOR","*");
                break;
            case 3:
                $wizard->SetVar("OPERATOR","/");
                break;
        }
        $wizard->SetVar("EXPRESSION",$wizard->GetVar("F_NUMBER").$wizard->GetVar("OPERATOR").$wizard->GetVar("S_NUMBER"));
        $this->content .= "<table class='wizard-data-table'>";
        $this->content .= "<tr><th align = 'right'><span class='wizard-required'>*</span>" . Loc::getMessage('WATCH_EXPRESSION') . "</th><td>";
        $this->content .= $wizard->GetVar("EXPRESSION")." = ?" . "</td></tr>";
        $this->content .= "</table>";
    }

    /** Метод, срабатывающий перед отправкой формы: обработка ошибок */
    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        if ($wizard->IsCancelButtonClick())
            return;
        $second_num = $wizard->GetVar("S_NUMBER");
        $operator = $wizard->GetVar("OPERATOR");
        if ($operator == "/" && intval($second_num) == 0)
        {
            $this->SetError(Loc::getMessage('ERROR_DIVISION_BY_ZERO'));
            $this->SetCancelStep("second_number");
            $this->SetCancelCaption(Loc::getMessage("SP_ERROR_CANCEL_CAPTION"));
        }
    }

}

/** Класс последнего шага: решение выражения и демонстрация результата */
class Solve extends CWizardStep
{
    /** Инициализация, добавление предыдущего шага, заголовка и подзаголовка, название кнопки отмены */
    function InitStep()
    {
        $this->SetStepID("solve");

        $this->SetTitle(Loc::getMessage("S_INIT_TITLE"));
        $this->SetTitle(Loc::getMessage("S_INIT_SUBTITLE"));

        $this->SetCancelStep("show_problem");
        $this->SetCancelCaption(Loc::getMessage("S_INIT_CANCEL_CAPTION"));
    }

    /** Метод вывогда: подсчет и вывод на экран результата, его запись в журнал событий */
    function ShowStep()
    {
        $wizard =& $this->GetWizard();
        $this->content .= "<table class='wizard-data-table'>";
        $this->content .= "<tr><th align = 'right'><span class='wizard-required'>*</span>" . Loc::getMessage('WATCH_EXPRESSION') . "</th><td>";
        $this->content .= $wizard->GetVar("EXPRESSION")." = ".
            eval ("return " . $wizard->GetVar("EXPRESSION") . ";").
            "</td></tr>";
        $this->content .= "</table>";

        /** Записываем результат в журнал событий */
        CEventLog::Add(array(
            "SEVERITY" => "INFO",
            "AUDIT_TYPE_ID" => "MASTER_ACTION",
            "MODULE_ID" => "WIZARDS",
            "ITEM_ID" => "custom::calc",
            "DESCRIPTION" => "Успешное завершение работы мастера. Посчитано выражение " .
                $wizard->GetVar("EXPRESSION") . " = " .
                eval ("return " . $wizard->GetVar("EXPRESSION") . ";"),
        ));
    }
    
}

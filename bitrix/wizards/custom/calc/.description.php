<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include(dirname(__FILE__) . '/version.php');
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

$arWizardDescription = array(
	"NAME" => Bitrix\Main\Localization\Loc::getMessage("MASTER_NAME"),
	"DESCRIPTION" => Bitrix\Main\Localization\Loc::getMessage("MASTER_DESCRIPTION"),
	"VERSION" => $arWizardVersion['VERSION'],
	"STEPS" => array("ActionSelect", "FirstNumber", "SecondNumber", "ShowProblem", "Solve")
);
?>
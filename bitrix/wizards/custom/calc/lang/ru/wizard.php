<?
$MESS["AS_INIT_TITLE"] = "Выбор действия";
$MESS["AS_INIT_SUBTITLE"] = "Выберите математическое действие";
$MESS["FN_INIT_TITLE"] = "Первое число";
$MESS["FN_INIT_SUBTITLE"] = "Введите первое число";
$MESS["AS_CHOOSE_ACTION"] = "Выберите действие";
$MESS["ERROR_INVALID_NUM"] = "Число введено неверно";
$MESS["FN_ENTER_NUM"] = "Введите первое число в это поле: ";
$MESS["SN_ENTER_NUM"] = "Введите второе число в это поле: ";
$MESS["SN_INIT_TITLE"] = "Второе число";
$MESS["SN_INIT_SUBTITLE"] = "Введите второе число";
$MESS["SP_INIT_TITLE"] = "Просмотр выражения";
$MESS["SP_INIT_SUBTITLE"] = "Получившееся выражение";
$MESS["WATCH_EXPRESSION"] = "Посмотрите получившееся выражение";
$MESS["SP_INIT_FINISH_CAPTION"] = "Посчитать";
$MESS["S_INIT_TITLE"] = "Завершение работы мастера";
$MESS["S_INIT_SUBTITLE"] = "Работа мастера успешно завершена!";
$MESS["S_INIT_CANCEL_CAPTION"] = "Готово";
$MESS["ERROR_DIVISION_BY_ZERO"] = "Ошибка! Деление на ноль!";
$MESS["SP_ERROR_CANCEL_CAPTION"] = "Ввести другое число";
$MESS["AS_ADDITION"] = "Сложение +";
$MESS["AS_SUBSTRACTION"] = "Вычитание -";
$MESS["AS_MULTIPLICATION"] = "Умножение *";
$MESS["AS_DIVISION"] = "Деление /";
?>
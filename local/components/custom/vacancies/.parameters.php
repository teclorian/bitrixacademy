<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
    return;
//получаем типы инфоблоков для меню
$arTypes = Array();
$db_iblock_type = CIBlockType::GetList(Array("SORT"=>"ASC"));
while($arRes = $db_iblock_type->Fetch())
    if($arIBType = CIBlockType::GetByIDLang($arRes["ID"], LANG))
        $arTypes[$arRes["ID"]] = $arIBType["NAME"];

//получаем список инфоблоков данного типа
$arIBlocks = Array("-"=>"-");
$iblocks = CIBlock::GetList(Array("SORT"=>"ASC"), Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $iblocks->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

//добавление инфоблоков в параметры

$arComponentParameters = array(
    "GROUPS" => array(
        "SETTINGS" => array(
            "NAME" => GetMessage("SETTINGS"),
            "SORT" => 10,
        ),
        'SECTION_SETTINGS' => array(
            'NAME' => GetMessage("CN_P_VACANCIES_SETTINGS"),
            'SORT' => 20
        ),
        'ELEMENT_SETTINGS' => array(
            'NAME' => GetMessage("CN_P_VACANCY_SETTINGS"),
            'SORT' => 30
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPES"),
            "TYPE" => "LIST",
            "VALUES" => $arTypes,
            "DEFAULT" => "-",
            "REFRESH" => "Y",
            "SORT" => 10,
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => "-",
            "REFRESH" => "Y",
            "SORT" => 20,
        ),
        "SEF_MODE" => Array(
            "vacancies" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VACANCIES"),
                "DEFAULT" => "vacancies/",
                "VARIABLES" => array(),
            ),
            "vacancy" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VACANCIES_DETAIL"),
                "DEFAULT" => "vacancies/#VACANT_ID#/",
                "VARIABLES" => array("VACANT_ID"),
            ),
            "resume" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VACANCIES_RESUME"),
                "DEFAULT" => "vacancies/#VACANT_ID#/resume",
                "VARIABLES" => array("VACANT_ID"),
            ),
        ),
        "CACHE_TIME"  =>  Array(
            "DEFAULT" => 3600,
        ),
    ),
);
CIBlockParameters::Add404Settings($arComponentParameters, $arCurrentValues);



?>

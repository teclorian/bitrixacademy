<?php
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\LoaderException;
use \Bitrix\Main\SystemException;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class CCustomVacancies extends CBitrixComponent
{
    /**
     * Проверка наличия модулей требуемых для работы компонента
     * @return bool
     * @throws LoaderException
     * @throws SystemException
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new SystemException('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }

    public function detectComponentPage () {
        $arDefaultUrlTemplates404 = [
            'vacancies'    => 'vacancies/',
            'vacancy' => 'vacancies/#VACANT_ID#/',
            'resume' => 'vacancies/#VACANT_ID#/resume/'
        ];
        $arDefaultVariableAliases404 = array(
            "vacancy" => array ("ELEMENT_ID" => "VACANT_ID"),
            "resume" => array ("ELEMENT_ID" => "VACANT_ID"),
        );
        $arDefaultVariableAliases = array(
            "vacancy" => array ("ELEMENT_ID" => "VACANT_ID"),
            "resume" => array ("ELEMENT_ID" => "VACANT_ID"),
        );
        $arComponentVariables = array(
            "VACANT_ID",
        );

        $SEF_FOLDER = '';
        $arUrlTemplates = array();
        $componentPage='';
        $arVariables = [];

        if ($this->arParams['SEF_MODE'] == 'Y') {
            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
                $arDefaultUrlTemplates404,
                $this->arParams['SEF_URL_TEMPLATES']
            );
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                $arDefaultVariableAliases404,
                $this->arParams['VARIABLE_ALIASES']
            );

            $componentPage = CComponentEngine::ParseComponentPath(
                $this->arParams['SEF_FOLDER'],
                $arUrlTemplates,
                $arVariables
            );

            if (strlen($componentPage) <= 0) {
                $componentPage = 'vacancies';
            }

            CComponentEngine::InitComponentVariables(
                $componentPage,
                $arComponentVariables,
                $arVariableAliases,
                $arVariables);
            $SEF_FOLDER = $this->arParams['SEF_FOLDER'];
        } else {
            $arVariables = [];

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                $arDefaultVariableAliases,
                $this->arParams['VARIABLE_ALIASES']
            );

            CComponentEngine::InitComponentVariables(
                false,
                $arComponentVariables,
                $arVariableAliases,
                $arVariables
            );

            $componentPage = '';
            if (isset($arVariables['VACANT_ID']) > 0 && strpos($arVariables['VACANT_ID'],"resume"))
            {
                $componentPage = 'resume';
            }
            elseif(intval($arVariables['VACANT_ID']) > 0)
            {
                $componentPage = 'vacancy';
            }
            else
            {
                $componentPage = 'vacancies';
            }
            $arVariables["VACANT_ID"] = str_ireplace("/resume","",$arVariables["VACANT_ID"]);
        }
        $this->arResult = [
            'FOLDER'        => $SEF_FOLDER,
            'URL_TEMPLATES' => $arUrlTemplates,
            'VARIABLES'     => $arVariables,
            'ALIASES'       => $arVariableAliases,
        ];
        return $componentPage;
    }
    public function executeComponent()
    {
        try {
            $this->_checkModules();
        } catch (SystemException $e) {
            ShowError($e);
        } catch (LoaderException $e) {
            ShowError($e);
        }
        $componentPage = $this->detectComponentPage();
        $this->includeComponentTemplate($componentPage);
        return parent::executeComponent();
    }
}
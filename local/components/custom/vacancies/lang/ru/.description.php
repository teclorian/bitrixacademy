<?
$MESS["IBLOCK_VACANCIES_NAME"] = "Вакансии";
$MESS["IBLOCK_VACANCIES_DESCRIPTION"] = "Раздел вакансий";
$MESS["T_IBLOCK_DESC_VACANCIES"] = "Вакансии";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES"] = "Использовать ЧПУ";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES_SECTION"] = "Страница раздела";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES_DETAIL"] = "Страница детального просмотра";
$MESS["BN_P_SECTION_ID_DESC"] = "Идентификатор раздела";
$MESS["VACANCY_ELEMENT_ID_DESC"] = "Идентификатор вакансии";
$MESS["VACANCIES_MENU_ID"] = "Дополнительные";

?>
<?
$MESS['SETTINGS'] = "Настройки компонента";
$MESS['IBLOCK_TYPES'] = "Типы инфоблоков";
$MESS['IBLOCK_LIST'] = "Список инфоблоков";
$MESS["CN_P_VACANCIES_SETTINGS"] = "Настройки списка";
$MESS["CN_P_VACANCY_SETTINGS"] = "Настройки детального просмотра";
$MESS["IBLOCK_TYPE"] = "Инфоблок";
$MESS["T_IBLOCK_DESC_PAGER_VACANCIES"] = "Вакансии";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES"] = "Страница общего списка";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES_RESUME"] = "Страница резюме";
$MESS["T_IBLOCK_SEF_PAGE_VACANCIES_DETAIL"] = "Страница детального просмотра";
$MESS["T_IBLOCK_DESC_FILTER_SETTINGS"] = "Настройки фильтра";
$MESS["T_IBLOCK_DESC_USE_FILTER"]="Показывать фильтр";
$MESS["T_IBLOCK_PROPERTY"] = "Свойства";
$MESS["T_IBLOCK_FILTER"] = "Фильтр";
$MESS["IBLOCK_FIELD"] = "Поля";
?>
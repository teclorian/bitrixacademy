<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="sb_nav">
<ul>
<?
foreach ($arResult['ITEMS'] as $key=>$item):
?>
    <li class="close"><span class="sb_showchild"></span><a href="#"><span><?=$key?></span></a>
    <ul>
        <hr>
        <?foreach ($item as $val):?>
        <li class="close"><?
            $this->AddEditAction($val['ID'],$val['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($val['ID'],$val['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('DELETE_CONFIRMATION', array("#ELEMENT#" => CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_NAME")))));
            ?>
            <div id="<?=$this->GetEditAreaId($val['ID']);?>">
                <h3><a href="<?=$val["DETAIL_PAGE_URL"];?>"><?=$val["NAME"]?></a></h3>
                <div>
                    <p><?=$val["PREVIEW_TEXT"]?></p><br>
                    <p>
                        <span>Необходимый стаж (лет): <?=$val["~PROPERTY_EXPERIENCE_VALUE"]?></span><br>
                        <span>Необходимое образование: <?=$val["~PROPERTY_EDUCATION_VALUE"]?></span><br>
                        <span>График работы: <?=$val["~PROPERTY_SCHEDULE_VALUE"]?></span>
                    </p>
                </div>
            </div>
        </li>
        <hr>
        <?endforeach;?>
    </ul>
    </li>
<?endforeach;?>
</ul>
</div>

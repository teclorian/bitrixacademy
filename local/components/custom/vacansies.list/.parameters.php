<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

//получаем типы инфоблоков для меню
$arTypes = Array();
$db_iblock_type = CIBlockType::GetList(Array("SORT"=>"ASC"));
while($arRes = $db_iblock_type->Fetch())
    if($arIBType = CIBlockType::GetByIDLang($arRes["ID"], LANG))
        $arTypes[$arRes["ID"]] = $arIBType["NAME"];

//получаем список инфоблоков данного типа
$arIBlocks = Array();
$iblocks = CIBlock::GetList(Array("SORT"=>"ASC"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"]));
while($arRes = $iblocks->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

//добавление инфоблоков в параметры

$arComponentParameters = array(
    "GROUPS" => array(
        "SETTINGS" => array(
            "NAME" => GetMessage("SETTINGS"),
            "SORT" => 10,
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "SETTINGS",
            "NAME" => GetMessage("IBLOCK_TYPES"),
            "TYPE" => "LIST",
            "VALUES" => $arTypes,
            "DEFAULT" => "-",
            "REFRESH" => "Y",
            "SORT" => 10,
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "SETTINGS",
            "NAME" => GetMessage("IBLOCK_LIST"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => "-",
            "REFRESH" => "Y",
            "SORT" => 20,
        ),
        "CACHE_TIME"  =>  Array(
            "DEFAULT" => 3600,
        ),
    ),
);

?>
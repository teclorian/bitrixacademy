<?php
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\LoaderException;
use \Bitrix\Main\SystemException;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class CCustomVacanciesList extends CBitrixComponent
{
    /**
     * Проверка наличия модулей требуемых для работы компонента
     * @return bool
     * @throws LoaderException
     * @throws SystemException
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new SystemException('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }

    private function _app ()
    {
        global $APPLICATION;
        return $APPLICATION;
    }

    private function _user()
    {
        global $USER;
        return $USER;
    }

    public function prepareResult()
    {
        //подготовка параметров

        $this->arParams['IBLOCK_ID'] = intval($this->arParams['IBLOCK_ID']);
        if($this->arParams['IBLOCK_ID']<=0)
            return;

        if(isset($this->arParams["IBLOCK_TYPE"]) && $this->arParams["IBLOCK_TYPE"]!='')
            $arFilter['IBLOCK_TYPE'] = $this->arParams["IBLOCK_TYPE"];

        $arSelect = Array(
            "ID",
            "NAME",
            "IBLOCK_SECTION_ID",
            "LANG_ID",
            "IBLOCK_ID",
            "PREVIEW_TEXT_TYPE",
            "PREVIEW_TEXT",
            "DETAIL_PAGE_URL",
            "PROPERTY_EXPERIENCE",
            "PROPERTY_SCHEDULE",
            "PROPERTY_EDUCATION",
        );
        $arFilter = Array(
            'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
            'ACTIVE' => 'Y',
            'IBLOCK_ACTIVE' => 'Y',
        );
        $arOrder = Array(
            'SORT' => 'ASC',
        );

        //Начало кеширования
        if($this->StartResultCache())
        {
            $this->arResult['ITEMS'] = Array();
            $arItems = CIBlockElement::GetList($arOrder, $arFilter, false,false,$arSelect);
            while($arResItems = $arItems->GetNext()){
                $sect = CIBlockSection::GetByID($arResItems["IBLOCK_SECTION_ID"])->Fetch(); //т.к. нам нужны названия разделов,
                // а GetList в CIBlocksSection не даёт элементы, приходится получать название раздела таким образом
                $sectName=$sect["NAME"];
                //формирование ссылки для кнопки для изменения элемента
                $arResItems["EDIT_LINK"] = '/bitrix/admin/iblock_element_edit.php?ID='.$arResItems["ID"].
                    '&type='.$this->arParams["IBLOCK_TYPE"].'&lang='.LANGUAGE_ID.'&IBLOCK_ID='.$this->arParams["IBLOCK_ID"].
                    '&find_section_section='.$arResItems["IBLOCK_SECTION_ID"].'&bxpublic=Y&from_module=iblock';
                //формирование ссылки для кнопки удаления элемента
                $urlDelete = CIBlock::GetAdminElementListLink($this->arParams["IBLOCK_ID"], array('action'=>'delete'));
                $urlDelete .= '&' . bitrix_sessid_get();
                $urlDelete .= '&ID='.(preg_match('/^iblock_list_admin\.php/', $urlDelete)? "E": "").$arResItems["ID"];
                $urlDelete = "/bitrix/admin/".$urlDelete;
                $arResItems['DELETE_LINK'] = $urlDelete;
                if($sectName !== NULL) {
                    $this->arResult['ITEMS'][$sectName][] = $arResItems;
                }
            }

            if(count($this->arResult['ITEMS'])<=0)
            {
                $this->AbortResultCache();
                @define("ERROR_404", "Y");
                return;
            }
            $this->IncludeComponentTemplate();
        }
    }

    public function setAdminButtons () {
        //добавление кнопок редактирования инфоблока
        $USER = $this->_user();
        $APPLICATION = $this->_app();
        if($USER->IsAuthorized())
        {
            if(
                $APPLICATION->GetShowIncludeAreas()
                || $this->arParams["SET_TITLE"]
                || isset($arResult[$arParams["BROWSER_TITLE"]])
            )
            {
                $arButtons = CIBlock::GetPanelButtons($this->arParams["IBLOCK_ID"], 0, $this->arParams["SECTION_ID"]);

                foreach ($arButtons as $key => $arButton){
                    unset($arButtons[$key]['add_section']);
                    unset($arButtons[$key]['edit_section']);
                    unset($arButtons[$key]['delete_section']);
                }

                if($APPLICATION->GetShowIncludeAreas())
                    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
            }
        }
    }
    public function executeComponent()
    {
        try {
            $this->_checkModules();
        } catch (SystemException $e) {
            ShowError($e);
        } catch (LoaderException $e) {
            ShowError($e);
        }
        $this->prepareResult();
        $this->setAdminButtons();
        return parent::executeComponent();
    }
}
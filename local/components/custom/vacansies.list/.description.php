<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_VACANCIES_LIST_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_VACANCIES_LIST_DESC"),
	"ICON" => "/images/vacancies_list.gif",
	"PATH" => array(
		"ID" => GetMessage("IBLOCK_VACANCIES_LIST_MENU_ID"),
		"SORT" => 1000,
		"CHILD" => array(
			"ID" => "vacancies",
			"NAME" => GetMessage("IBLOCK_VACANCIES_LIST_COMPONENT_NAME"),
			"SORT" => 30,
		),
	),
);

?>
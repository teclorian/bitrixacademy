<?
use Bitrix\Main\Mail\Event;
use \Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
EventManager::getInstance()->addEventHandler(
    "iblock",
    "OnBeforeIBlockElementUpdate",
    "CIBlockHandler::OnBeforeIBlockElementUpdateHandler");
EventManager::getInstance()->addEventHandler(
    "iblock",
    "OnBeforeIBlockElementDelete",
    "CIBlockHandler::OnBeforeIBlockElementDeleteHandler");
EventManager::getInstance()->addEventHandler(
    "main",
    "OnBeforeUserUpdate",
    "CIBlockHandler::OnBeforeUserUpdateHandler");

class CIBlockHandler
{
    function OnBeforeIBlockElementUpdateHandler(&$arFields){
        if($arFields["IBLOCK_ID"] == IBLOCK_NEWS_ID){
        $date = DateTime::createFromFormat('d.m.Y', $arFields["ACTIVE_FROM"]);
        $now = new DateTime();
            if($arFields["ACTIVE"] == "N" && $date->diff($now)->format('%a') < 3 ){
                global $APPLICATION;
                $APPLICATION -> throwException("Вы деактивировали свежую новость");
                return false;
            }
        }
    }
    function OnBeforeIBlockElementDeleteHandler($ID){
        try {
        Loader::includeModule("iblock");
        } catch (\Bitrix\Main\LoaderException $e) {
            ShowError("Модуль инфоблоков не установлен " . $e);
        }
        $res = CIBlockElement::GetByID($ID);
        if($arRes = $res->GetNext())
        {
            if($arRes["SHOW_COUNTER"]>=1){
                (new CIBlockElement())->Update($ID, ['ACTIVE'=>'N']);
                $GLOBALS['DB']->Commit();
                $GLOBALS['APPLICATION']->throwException("Товар деактивирован. Количество просмотров: ". $arRes['SHOW_COUNTER']);
                return false;
            }
        }
    }
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $arGroupsBefore=array();
        $arGroupsAfter=array();
        $arGroupsBefore = \Bitrix\Main\UserGroupTable::getList(array(
            'filter' => array('USER_ID' => $arFields["ID"], 'GROUP.ACTIVE'=>'Y'),
            'select' => array('GROUP_ID','GROUP_CODE'=>'GROUP.STRING_ID'),
            'order' => array('GROUP.C_SORT'=>'ASC'),
        ));
        foreach($arFields["GROUP_ID"] as $aGroup){
            $arGroupsAfter[] = $aGroup["GROUP_ID"];
        }
        if((!in_array(CONTENT_GROUP_ID, $arGroupsBefore)) && (in_array(CONTENT_GROUP_ID, $arGroupsAfter))){
            $DBUsers = \Bitrix\Main\UserGroupTable::getList(array(
                'select' => array("NAME" => "USER.NAME", "LAST_NAME" => "USER.LAST_NAME", "EMAIL" => "USER.EMAIL"),
                'filter' => array("GROUP_ID" => CONTENT_GROUP_ID),
                'order' => array('USER.ID'=>'DESC'),
            ));

            while ($arUser = $DBUsers->Fetch()) {
                $arSendFields=array(
                    "MEMBER_EMAIL" => $arUser["EMAIL"],
                    "MEMBER_NAME" => $arUser["NAME"]." ".$arUser["LAST_NAME"],
                    "NEW_NAME" => $arFields["NAME"]." ".$arFields["LAST_NAME"],
                );

                Event::send(array(
                    "EVENT_NAME" => "NEW_CONTENT_USER",
                    "LID" => "s1",
                    "C_FIELDS" => $arSendFields,
                ));
            }
        }
    }
}

?>
<?php

/** Константа, в которой содержится ID инфоблока "акции" */
define("IBLOCK_STOCKS_ID",6);
/** Константа, в которой содержится ID инфоблока "продукция" */
define("IBLOCK_PRODUCTS_ID",2);
/** Константа, в которой содержится ID инфоблока "новости" */
define("IBLOCK_NEWS_ID",1);
/** Константа, в которой содержится ID группы "контент-редакторов" */
define("CONTENT_GROUP_ID",5);
/** Константа, в которой содержится ID веб-формы "резюме" */
define("WEB_FORM_RESUME_ID",1);

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/functions_dump.php"))
{
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/functions_dump.php");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/event_handlers.php"))
{
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers.php");
}
?>
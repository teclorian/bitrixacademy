<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParameters = Array(
    "PARAMETERS"=> Array(
        "TEXT_MSG"=> Array(
            "NAME" => "Текст формы",
            "TYPE" => "STRING",
            "DEFAULT" => "Количество резюме за сегодня: #TODAY# и за всё время: #ALL_TIME#",
        ),
        "WEBFORM_ID"=> Array(
            "NAME" => "ID Веб-формы",
            "TYPE" => "STRING",
            "DEFAULT" => WEB_FORM_RESUME_ID,
        ),
    ),
    "USER_PARAMETERS" => Array(
        "RESULT_PAGE"=> Array(
            "NAME" => "Ссылка на страницу результатов",
            "TYPE" => "STRING",
            "DEFAULT" => "http://bondarenko.ivsupport.ru/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID=".WEB_FORM_RESUME_ID,
        ),
    ),
);

?>

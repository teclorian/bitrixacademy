<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

try {
    \Bitrix\Main\Loader::includeModule("form");
} catch (\Bitrix\Main\LoaderException $e) {
    ShowError("Модуль 'формы' не установлен");
    return;
}

if (!isset($arGadget["SETTINGS"]["WEBFORM_ID"]))
{
    ShowMessage("Укажите ID инфоблока");
    return;
}

$by = "s_timestamp";
$order = "desc";
$isFiltered = false;
$rs_results = CFormResult::GetList (
    $arGadget["SETTINGS"]["WEBFORM_ID"],
    $by,
    $order,
    array("TIMESTAMP_2" => date(FormatDateFromDB(CSite::GetDateFormat("FULL")), time())),
    $isFiltered,
    "N"
    );
$today = date(FormatDateFromDB(CSite::GetDateFormat("SHORT")), time());
$formsToday = 0;
$formsAll = 0;
if($isFiltered)
    while($rs = $rs_results->Fetch())
    {
        if($rs["DATE_CREATE"]>=$today)
        {
            $formsToday++;
            $formsAll++;
        }
        else
            $formsAll++;

    }
?>

<table>
<span>Резюме отправлено за сегодня:
    <a href="<?=$arGadget["SETTINGS"]["PAGE_URL"]?>
    &set_filter=Y&adm_filter_applied=0&find_date_create_1_FILTER_PERIOD=exact&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=<?=$today?>
    &find_date_create_2=<?=$today?>">
        <?=$formsToday?></a>
</span>
    <br>
<span>За всё время: <a href="<?=$arGadget["SETTINGS"]["PAGE_URL"]?>"><?=$formsAll?></a></span>
</table>
<?
$arTempID = array();
foreach($arResult["ITEMS"] as $elem){
	$arTempID[] = $elem["PROPERTIES"]["LINK"]["VALUE"];
}
$arSort = false;
$arFilter = array(
	"IBLOCK_ID" => IBLOCK_PRODUCTS_ID,
	"ACTIVE" => "Y",
	"ID" => $arTempID,
);
$arGroupBy = false;
$arNavStartParams = array("nTopCount" => count($arResult["ITEMS"]));
$arSelect = array("ID", "NAME", "PRICE","DETAIL_PAGE_URL");
$BDRes = CIBlockElement::GetList(
$arSort,
$arFilter,
$arGroupBy,
$arNavStartParams,
$arSelect);
$arResult["PRODUCTS"] = array();
while ($arRes = $BDRes->GetNext()){
	$arResult["PRODUCTS"][$arRes["ID"]] = $arRes;
}


?>
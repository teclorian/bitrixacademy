<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	$().ready(function(){
		$(function(){
			$('#slides').slides({
				preload: false,
				generateNextPrev: false,
				autoHeight: true,
				play: 4000,
				effect: 'fade'
			});
		});
	});
</script>
<div class="sl_slider" id="slides">
	<div class="slides_container" style="overflow: hidden; position: relative; display: block;">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<div>
			<div>
				<?
				$resize_image = CFile::ResizeImageGet($arItem["~DETAIL_PICTURE"],
				Array("width" => $arParams["IMAGE_SIZE"], "height" => $arParams["IMAGE_SIZE"]),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);
				?>
				<?if(is_array($arItem["DETAIL_PICTURE"])):?>
				<img src="<?=$resize_image["src"]?>" alt="" />
				<?endif;?>
				<h2><a href="<?=$arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["DETAIL_PAGE_URL"];?>"
					   title="<?=$arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"]?>">
						<?=$arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"]?>
					</a>
				</h2>
				<p><?echo $arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"]." 
				всего за ".$arItem["DISPLAY_PROPERTIES"]["price"]["DISPLAY_VALUE"]." рублей!"?></p>
				<a href="<?=$arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["DETAIL_PAGE_URL"];?>"
				   title="<?=$arResult["PRODUCTS"][$arItem["PROPERTIES"]["LINK"]["VALUE"]]["NAME"]?>" class="sl_more">
					Подробнее &rarr;</a>
			</div>
		</div>
		<?endforeach;?>
	</div>
</div>
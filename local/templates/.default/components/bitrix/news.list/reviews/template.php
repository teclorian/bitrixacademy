<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<script type="text/javascript" >
	$(document).ready(function(){

		$("#foo").carouFredSel({
			items:2,
			prev:'#rwprev',
			next:'#rwnext',
			scroll:{
				items:1,
				duration:2000
			}
		});
	});
</script>

<div class="rw_reviewed">
	<div class="rw_slider">
		<h4>Отзывы</h4>
		<ul id="foo">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<li>
	<div class="rw_message">
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="rw_avatar"/>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<span class="rw_name"><?echo $arItem["NAME"]?></span>
		<?endif;?>

		<span class="rw_job">
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

				<?=$arProperty["DISPLAY_VALUE"];?>
		<?endforeach;?>
		</span>

		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<p><?echo $arItem["PREVIEW_TEXT"];?></p>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div class="clearboth"></div>
		<?endif?>
		<div class="rw_arrow"></div>
	</div>
	</li>
<?endforeach;?>
		</ul>
		<div id="rwprev"></div>
		<div id="rwnext"></div>
		<a href="/company/reviews/" class="rw_allreviewed">Все отзывы</a>
	</div>
</div>

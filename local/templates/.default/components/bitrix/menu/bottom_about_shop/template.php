<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult)) return;
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
<div class="ft_about">
<h4><?=\Bitrix\Main\Localization\Loc::getMessage('ABOUT_SHOP')?></h4>
			<ul id="footer-links">
<?
foreach($arResult as $arItem):
?>
				<li>
					<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				</li>
<?
endforeach;
?>
			</ul>
</div>
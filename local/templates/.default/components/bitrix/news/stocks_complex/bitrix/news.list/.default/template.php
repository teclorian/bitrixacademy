<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="ps_head"><a class="ps_head_link" href="..<?=substr($arItem["PROPERTY_LINK_DETAIL_PAGE_URL"], 1)?>"><h2 class="ps_head_h"><?=$arItem["PROPERTY_LINK_NAME"]?></h2></a></div>
    <div class="ps_content">
        <?

        $resize_image = CFile::ResizeImageGet($arItem["PROPERTY_LINK_DETAIL_PICTURE"],
            Array("width" => $arParams["IMAGE_SIZE"], "height" => $arParams["IMAGE_SIZE"]),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);
        ?>
        <img src="<?=$resize_image['src'];?>" align="left" alt=""/>
        <?=$arItem["~PROPERTY_LINK_PREVIEW_TEXT"]?>
        <p>
            <?echo $arItem["PROPERTY_LINK_NAME"]." всего за ".$arItem["DISPLAY_PROPERTIES"]["price"]["DISPLAY_VALUE"]." рублей!"?>
        </p>
        <div style="clear:both"></div>
        <p class="ps_backnewslist">
        <a href="..<?=substr($arItem["PROPERTY_LINK_DETAIL_PAGE_URL"], 1)?>">Подробнее &rarr;</a>
        </p>
    </div>

<?endforeach;?>
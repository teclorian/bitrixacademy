<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
$sectCODE=$arResult["ORIGINAL_PARAMETERS"]["SECTION_CODE"];
$arSort = false;
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_PRODUCTS_ID,
    "ACTIVE" => "Y",
    "SECTION_CODE" => $sectCODE,
);
$arGroupBy =  array("PROPERTY"=>"PROPERTY_MATERIAL");
$arNavStartParams = false;
$arSelectFields = false;
$BDRes = CIBlockElement::GetList(
    $arSort,
    $arFilter,
    $arGroupBy,
    $arNavStartParams,
    $arSelectFields);

$arResult["MATERIALS"] = array();
while ($arRes = $BDRes->GetNext()){
    $arResult["MATERIALS"][] = $arRes;
}
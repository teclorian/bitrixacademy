<div class="ft_footer">
	<div class="ft_container">
			<?php
			$APPLICATION->IncludeComponent("bitrix:menu", "bottom_about_shop", array(
				"ROOT_MENU_TYPE" => "bottom_about_shop",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "bottom_about_shop",
		"DELAY" => "N"
			),
				false
			);
			?>
		<div class="ft_catalog">
			<h4>Каталог товаров</h4>
			<ul>
				<li><a href="">Кухни</a></li>
				<li><a href="">Кровати и кушетки</a></li>
				<li><a href="">Гарнитуры</a></li>
				<li><a href="">Тумобчки и прихожие</a></li>
				<li><a href="">Спальни и матрасы</a></li>
				<li><a href="">Аксессуары</a></li>
				<li><a href="">Столы и стулья</a></li>
				<li><a href="">Каталоги мебели</a></li>
				<li><a href="">Раскладные диваны</a></li>
				<li><a href="">Кресла</a></li>
			</ul>

		</div>
		<div class="ft_contacts">
			<h4><?=\Bitrix\Main\Localization\Loc::getMessage('CONTACT_INFO')?></h4>
			<!-- vCard        http://help.yandex.ru/webmaster/hcard.pdf      -->

			<p class="vcard">
						<span class="adr">
							<span class="street-address">
									<?php
									$APPLICATION->IncludeComponent(
										"bitrix:main.include",
										".default",
										array(
											"AREA_FILE_SHOW" => "sect",
											"AREA_FILE_SUFFIX" => "address",
											"COMPONENT_TEMPLATE" => ".default",
											"AREA_FILE_RECURSIVE" => "Y",
											"EDIT_TEMPLATE" => ""
										),
										false
									);
									?>
								</span>
						</span>
				<span class="tel"><?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => "/local/templates/.default/includes/phone.php"
						)
					);?></span>
				<strong>Время работы:</strong> <br/> <span class="workhours">ежедневно с 9-00 до 18-00</span><br/>
			</p>
			<ul class="ft_solcial">
				<li><a href="" class="fb"></a></li>
				<li><a href="" class="tw"></a></li>
				<li><a href="" class="ok"></a></li>
				<li><a href="" class="vk"></a></li>
			</ul>
			<div class="ft_copyright">© 2000 - 2012 "<?=\Bitrix\Main\Localization\Loc::getMessage('SHOP_NAME'); //почему не работает?>" </div>


		</div>

		<div class="clearboth"></div>
	</div>
</div>
</div>
</body>
</html>
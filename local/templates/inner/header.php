<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?php
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<?php $APPLICATION->ShowHead();?>
	<title><?php $APPLICATION->ShowTitle();?></title>
	<?
    use Bitrix\Main\Page\Asset;
    Asset::getInstance()->addJs('/local/templates/.default/js/jquery-1.8.2.min.js');
    Asset::getInstance()->addJs('/local/templates/.default/js/functions.js');
    Asset::getInstance()->addCss("/local/templates/.default/template_styles.css");
	?>

	<link rel="shortcut icon" type="image/x-icon" href="/local/templates/.default/favicon.ico"/>
	<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
</head>
<body>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/local/templates/.default/includes/header_include.php"); ?>
	<!--- // end header area --->
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"nav",
		Array(
			"COMPONENT_TEMPLATE" => "nav",
			"PATH" => "",
			"SITE_ID" => "s1",
			"START_FROM" => "0"
		)
	);?>
	<div class="main_container page">
		<div class="mn_container">
			<div class="mn_content">
				<div class="main_post">
					<div class="main_title">
                        <?=$APPLICATION->ShowViewContent("news_detail_date");?>
						<p class="title"><?php $APPLICATION->ShowTitle(false);?></p>

					</div>